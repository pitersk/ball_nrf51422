/*
 * BMC150_driver.c
 *
 *  Created on: 30.04.2017
 *      Author: piter
 */

#include "BMC150_driver.h"


void bmc150_get_data(int16_t *acc_x_val, int16_t *acc_y_val, int16_t *acc_z_val,
		int16_t *mag_x_val, int16_t *mag_y_val, int16_t *mag_z_val){

	uint8_t rx_buffer[BMC150_XYZ_DATA_LENGTH];

	twi_read_register(BMC150_ACC_I2C_SLAVE_ADDRESS, BMC150_ACC_X_LSB_ADDR, rx_buffer, BMC150_XYZ_DATA_LENGTH);

	/* Add two LSB and MSB values to create 16 bit readout */
	*acc_x_val = (rx_buffer[1]<<8) | rx_buffer[0];
	*acc_y_val = (rx_buffer[3]<<8) | rx_buffer[2];
	*acc_z_val = (rx_buffer[5]<<8) | rx_buffer[4];

	twi_read_register(BMC150_MAG_I2C_SLAVE_ADDRESS, BMC150_MAG_X_LSB_ADDR, rx_buffer, BMC150_XYZ_DATA_LENGTH);

	/* Add two LSB and MSB values to create 16 bit readout */
	*mag_x_val = (rx_buffer[1]<<8) | (rx_buffer[0] >> 3);
	*mag_y_val = (rx_buffer[3]<<8) | (rx_buffer[2] >> 3);
	*mag_z_val = (rx_buffer[5]<<8) | rx_buffer[4];

}

void bmc150_init_mag_normal_mode() {

	uint8_t tx_data = 0x01; // Power Bit on in magnetometer power control register
	twi_write_register(BMC150_MAG_I2C_SLAVE_ADDRESS,BMC150_POWER_CONTROL, &tx_data, 1);

	tx_data = 0x00; // Set magnetometer to normal mode
	twi_write_register(BMC150_MAG_I2C_SLAVE_ADDRESS,BMC150_CONTROL, &tx_data, 1);

}


