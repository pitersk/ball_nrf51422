/*
 * global_defines.h
 *
 *  Created on: 27.04.2017
 *      Author: piter
 */

#ifndef GLOBAL_DEFINES_H_
#define GLOBAL_DEFINES_H_


typedef enum { STATE_INVALID, STATE_PLAY, STATE_SEARCH, STATE_INIT, STATE_CHARGE, STATE_ERROR, STATE_IDLE, STATE_STANDBY, STATE_CHANGE_INTERVAL,
				STATE_COLOR_CHANGE, STATE_TRIGGER_BALL} ball_state_t;



#endif /* GLOBAL_DEFINES_H_ */
