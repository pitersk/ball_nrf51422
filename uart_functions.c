/*
 * uart_functions.c
 *
 *  Created on: 11 maj 2016
 *      Author: Piotrek
 */
#include "uart_functions.h"


void uart_error_handle(app_uart_evt_t * p_event)
{
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
}


void uart_init(){
    uint32_t err_code;
	const app_uart_comm_params_t comm_params =
	      {
	    		  UART_RX_PIN,
				  UART_TX_PIN,
	          10,
	          11,
	          APP_UART_FLOW_CONTROL_DISABLED,
	          false,
	          UART_BAUDRATE_BAUDRATE_Baud9600
	      };

	    APP_UART_FIFO_INIT(&comm_params,
	                         UART_RX_BUF_SIZE,
	                         UART_TX_BUF_SIZE,
	                         uart_error_handle,
	                         APP_IRQ_PRIORITY_LOW,
	                         err_code);

	    APP_ERROR_CHECK(err_code);
}

void uart_send_string(char* message){
	char buffer[20];
	uint8_t length = sprintf(buffer, "%s", message);
	int i;
	for(i=0 ; i<length; i++){
		while(app_uart_put((uint8_t)buffer[i]) != NRF_SUCCESS);
	}
}

void uart_test(){

//	while(app_uart_get(&cr) != NRF_SUCCESS);

	uart_send_string("hello world");
}



