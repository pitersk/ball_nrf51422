/*
 * twi_functions.h
 *
 *  Created on: 18.04.2017
 *      Author: piter
 */

#ifndef TWI_FUNCTIONS_H_
#define TWI_FUNCTIONS_H_

#include "stdbool.h"
#include "stddef.h"
#include "string.h"


#include "app_util_platform.h"
#include "nrf.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"

//#define BQ27421_SCL_PIN	5
//#define BQ27421_SDA_PIN 4
//#define BQ27421_TWI_READ_ADDR (0xAB)
//#define BQ27421_TWI_WRITE_ADDR (0xAA)

/* TWI instance. */
const nrf_drv_twi_t twi_instance;

#define I2C_SENSORS_SCL_PIN 5
#define I2C_SENSORS_SDA_PIN 4

void twi_init (void);

void twi_read_register(uint8_t slave_address, uint8_t register_address, uint8_t *read_data, uint8_t data_length);
void twi_write_register(uint8_t slave_address, uint8_t register_address, uint8_t *data, uint8_t data_length);


#endif /* TWI_FUNCTIONS_H_ */
