/*
 * state_definitions.c
 *
 *  Created on: 17.04.2017
 *      Author: piter
 */


#include "state_definitions.h"


volatile ball_state_t client_next_state_request = STATE_INVALID;
volatile ball_state_t current_state = STATE_INIT;

volatile uint8_t client_next_sensor_data_interval_ms_request = SENSOR_TIMER_INITIAL_INTERVAL_MS;
volatile uint8_t client_next_battery_data_interval_s_request = BATTERY_TIMER_INITIAL_INTERVAL_S;


ball_state_t do_state_function(ball_state_t state) {
	ball_state_t next_state = state;
	if( state != STATE_INIT){
		stopSensorTimer();
		buzzerOff();
	}

	switch(state){
	case STATE_INIT:
		next_state = do_state_init();
		break;
	case STATE_CHARGE:
		next_state = STATE_PLAY;
		break;
	case STATE_ERROR:
//		TODO
		break;
	case STATE_IDLE:
//		TODO
		break;
	case STATE_PLAY:
		next_state = do_state_play();
		break;
	case STATE_SEARCH:
//		TODO
		break;
	case STATE_STANDBY:
//		TODO
		break;
	case STATE_CHANGE_INTERVAL:
		next_state = do_state_change_interval();
		break;
	case STATE_COLOR_CHANGE:
		next_state = STATE_STANDBY;
		break;
	default:
		break;
	}

	return next_state;
}

ball_state_t do_state_init(){
	uint32_t err_code;
	init_PWM();
	enable_PWM();

	nrf_gpio_cfg_input(27, NRF_GPIO_PIN_NOPULL);

	ble_stack_init();
	gap_params_init();
	advertising_init();
	services_init();

	timersInit();
	buzzerInit();
	twi_init();

	initSensors();
	configFuelGauge();

	startBatteryTimer(battery_data_interval_s);


	err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
	APP_ERROR_CHECK(err_code);
	return STATE_PLAY;
}

ball_state_t do_state_play(){
	stopSensorTimer();
	startSensorTimer(sensor_data_interval_ms);
	return STATE_PLAY;
}

ball_state_t do_state_search( ){
	return STATE_SEARCH;
}
ball_state_t do_state_charge( ){
	return STATE_CHARGE;
}
ball_state_t do_state_error( ) {
	return STATE_ERROR;
}

ball_state_t do_state_change_interval(){
	sensor_data_interval_ms = client_next_sensor_data_interval_ms_request;
	battery_data_interval_s = client_next_battery_data_interval_s_request;
	stopBatteryTimer();
	startBatteryTimer(battery_data_interval_s);
	return STATE_PLAY;
}

ball_state_t do_state_trigger_ball(){
	return STATE_TRIGGER_BALL;
}


