/*
 * NRF51_SPI.c
 *
 *  Created on: 10.04.2017
 *      Author: piter
 */

#include "NRF51_SPI.h"


void spi_master_init(nrf_drv_spi_t const * p_instance, bool lsb)
{
    uint32_t err_code;

    nrf_drv_spi_config_t config =
    {
        .ss_pin       = NRF_DRV_SPI_PIN_NOT_USED,
        .irq_priority = APP_IRQ_PRIORITY_LOW,
        .orc          = 0xCC,
        .frequency    = NRF_DRV_SPI_FREQ_500K,
        .mode         = NRF_DRV_SPI_MODE_3,
        .bit_order    = (lsb ?
            NRF_DRV_SPI_BIT_ORDER_LSB_FIRST : NRF_DRV_SPI_BIT_ORDER_MSB_FIRST),
    };

    #if (SPI0_ENABLED == 1)
    if (p_instance == &m_spi_master)
    {
        config.sck_pin  = SPIM0_SCK_PIN;
        config.mosi_pin = SPIM0_MOSI_PIN;
        config.miso_pin = SPIM0_MISO_PIN;
        err_code = nrf_drv_spi_init(p_instance, &config,NULL); //spi_master_0_event_handler

        APP_ERROR_CHECK(err_code);
    }
    else
    {
        // Do nothing
    }
    #endif // (SPI0_ENABLED == 1)
}

void spiInit(){
	spi_master_init(&m_spi_master, true);
}

int8_t NRF51_SPI_writeRegister(uint8_t reg_addr, uint8_t * p_data, uint8_t nr_of_bytes, uint8_t cs_pin) {

    uint32_t err_code;

    uint8_t m_tx_data_spi[nr_of_bytes+1];

    memcpy(m_tx_data_spi+1,p_data,nr_of_bytes);

    // Set multiple bytes transfer if needed

    if(nr_of_bytes > 1)
    {
        m_tx_data_spi[0] = (reg_addr|0x40);
    }
    else
    {
        m_tx_data_spi[0] = reg_addr;
    }

    // Pull Chip Select line low
    nrf_gpio_pin_clear(cs_pin);

    // Transfer data
    err_code = nrf_drv_spi_transfer(&m_spi_master, m_tx_data_spi,
                        sizeof(m_tx_data_spi), NULL, 0);

    nrf_gpio_pin_set(cs_pin);

    if( err_code != NRF_SUCCESS){
        APP_ERROR_CHECK(err_code);
        return -1;
    }

    return 0;
}

int8_t NRF51_SPI_readRegister(uint8_t address, uint8_t * p_data, uint8_t nr_of_bytes, uint8_t cs_pin) {

	uint32_t err_code;

    uint8_t m_tx_data_spi;

    if(nr_of_bytes > 1)
    {
        m_tx_data_spi = (0x80|address)|0x40;
    }
    else
    {
        m_tx_data_spi = 0x80|address;
    }

    // Pull Chip Select line low
    nrf_gpio_pin_clear(cs_pin);

    err_code = nrf_drv_spi_transfer(&m_spi_master, &m_tx_data_spi,
    		nr_of_bytes, p_data, nr_of_bytes);

    // Set Chipselect line high
    nrf_gpio_pin_set(cs_pin);

    if( err_code != NRF_SUCCESS){
        APP_ERROR_CHECK(err_code);
        return -1;
    }

    return 0;



}


