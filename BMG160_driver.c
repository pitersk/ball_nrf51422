/*
 * BMG160_driver.c
 *
 *  Created on: 10.04.2017
 *      Author: piter
 */
#include "BMG160_driver.h"



void bmg160_get_data_XYZ(int16_t *x_val, int16_t *y_val, int16_t *z_val){

	uint8_t rx_buffer[6];
	twi_read_register(BMG160_I2C_SLAVE_ADDRESS, BMG160_RATE_X_LSB_BIT__REG, rx_buffer, BMG160_XYZ_DATA_LENGTH);

	/* Add two LSB and MSB values to create 16 bit readout */
	*x_val = (rx_buffer[1]<<8) | rx_buffer[0];
	*y_val = (rx_buffer[3]<<8) | rx_buffer[2];
	*z_val = (rx_buffer[5]<<8) | rx_buffer[4];

}


void bmg160_get_range_reg( uint8_t *gyro_range) {
	twi_read_register(BMG160_I2C_SLAVE_ADDRESS, BMG160_RANGE_ADDR_RANGE__REG, gyro_range, BMG160_GENERAL_REG_LENGHT );
}
void bmg160_set_range_reg( uint8_t *gyro_range) {
	twi_write_register(BMG160_I2C_SLAVE_ADDRESS, BMG160_RANGE_ADDR_RANGE__REG, gyro_range, BMG160_GENERAL_REG_LENGHT);
}
void bmg160_get_bw( uint8_t *bandwidth) {
	twi_read_register(BMG160_I2C_SLAVE_ADDRESS, BMG160_BW_ADDR__REG, bandwidth, BMG160_GENERAL_REG_LENGHT );
}
void bmg160_set_bw( uint8_t *bandwidth) {
	twi_write_register(BMG160_I2C_SLAVE_ADDRESS, BMG160_BW_ADDR__REG, bandwidth, BMG160_GENERAL_REG_LENGHT);
}




