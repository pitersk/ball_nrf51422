/*
 * gauge_functions.h
 *
 *  Created on: 15 cze 2017
 *      Author: piter
 */

#ifndef GAUGE_FUNCTIONS_H_
#define GAUGE_FUNCTIONS_H_

/*
 * gauge_functions.c
 *
 *  Created on: 15 cze 2017
 *      Author: piter
 */

#define MAX_ATTEMPTS	5

#define DESIGN_CAPACITY		180										//[mAh]
#define NOMINAL_VOLTAGE		3.6											//[V]
#define DESIGN_ENERGY		(DESIGN_CAPACITY * NOMINAL_VOLTAGE)			//[mWh]
#define TERMINATE_VOLTAGE	2750										//[mV]
#define TAPER_CURRENT		100											//[mA]
#define TAPER_RATE			(DESIGN_CAPACITY / (0.1 * TAPER_CURRENT))	//[0.1h]

#define GAUGE_DEVICE_ADDRESS 0x55

#define SET_CFGUPDATE 	0x0013
#define SOFT_RESET 		0x0042
#define GAUGE_RESET		0x0041
#define SET_HIBERNATE	0x0011

#define CMD_TEMP		0x02
#define CMD_VOLT		0x04
#define CMD_FLAGS 		0x06
#define CMD_RM			0x0C
#define CMD_FCC			0x0E
#define CMD_SOC			0x1C
#define CMD_SOH			0x20

#define CMD_DATA_CLASS 	0x3E
#define CMD_DATA_BLOCK 	0x3F
#define CMD_BLOCK_DATA	0x40
#define CMD_CHECK_SUM 	0x60

#define DC_STATE	0x52
#define DC_STATE_LENGTH	41

#define CFGUPD 	0x0010
#define ITPOR	0x0020


void batteryTimerHandler();
void configFuelGauge();
// nI2C = handle to your I2C driver (if required)
// nRegister = gauge register
// pData = pointer to data block which will hold the data from the gauge
// nLength = length
void i2c_read(int nI2C, unsigned char nRegister, unsigned char *pData, unsigned char nLength);

// nI2C = handle to your I2C driver (if required)
// nRegister = gauge register
// pData = pointer to data block which holds the data for the gauge
// nLength = length
void i2c_write(int nI2C, unsigned char nRegister, unsigned char *pData, unsigned char nLength);

// issue a control command (nSubCmd = sub command) to the gauge
unsigned int gauge_control(int nI2C, unsigned int nSubCmd);

// read 2 bytes from the gauge (nCmd = command)
unsigned int gauge_cmd_read(int nI2C, unsigned char nCmd);

// write 2 bytes to the gauge (nCmd = command, nData = 16 bit parameter)
unsigned int gauge_cmd_write(int nI2C, unsigned char nCmd, unsigned int nData);
#define MAX_ATTEMPTS 5
// exit CFG_UPDATE mode with one of the supported exit commands (e.g. SOFT_RESET)
int gauge_exit(int nI2C, unsigned int nCmd);

// enter CFG_UPDATE mode
int gauge_cfg_update(int nI2C);

// read a data class.
// nDataClass = data class number
// pData = raw data (for the whole data class)
// nLength = length of the whole data class
int gauge_read_data_class(int nI2C, unsigned char nDataClass, unsigned char *pData, unsigned char nLength);

unsigned char check_sum(unsigned char *pData, unsigned char nLength);

// write a data class.
// nDataClass = data class number
// pData = raw data (for the whole data class)
// nLength = length of the whole data class
int gauge_write_data_class(int nI2C, unsigned char nDataClass, unsigned char *pData, unsigned char nLength);
void print_data(unsigned char *pData, unsigned int nLength);









#endif /* GAUGE_FUNCTIONS_H_ */
