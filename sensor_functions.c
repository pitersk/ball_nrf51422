/*
 * sensor_functions.c
 *
 *  Created on: 27.04.2017
 *      Author: piter
 */

#include "sensor_functions.h"
#include "custom_peripheral_service.h"
#include "ble_functions.h"


void initSensors() {
	bmc150_init_mag_normal_mode();
}

void sensorTimerHandler() {
	uint32_t err_code;
	uint8_t new_data[SENSOR_CHARACTERISTIC_SIZE];
	sensor_data_t sensor_data;
	getSensorData(&sensor_data);
	// Copy  sensor_data_ struct to uint8_t array
	memcpy(new_data, &sensor_data, SENSOR_CHARACTERISTIC_SIZE);
	err_code = service_update_sensor_char(m_conn_handle, new_data);
	APP_ERROR_CHECK(err_code);
}

void getSensorData(sensor_data_t * sensor_data){
	bmg160_get_data_XYZ(&(sensor_data->bmg160X), &(sensor_data->bmg160Y), &(sensor_data->bmg160Z));
	bmc150_get_data(&(sensor_data->bmc_acc_150X), &(sensor_data->bmc_acc_150Y), &(sensor_data->bmc_acc_150Z), &(sensor_data->bmc_mag_150X),
			&(sensor_data->bmc_mag_150Y), &(sensor_data->bmc_mag_150Z));
}
