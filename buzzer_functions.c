/*
 * buzzer_functions.c
 *
 *  Created on: 4 maj 2017
 *      Author: piter
 */

#include "buzzer_functions.h"


void buzzerInit(){
	nrf_gpio_cfg_output(BUZ_PIN);
}

void buzzerOn() {
	nrf_gpio_pin_set(BUZ_PIN);
}
void buzzerOff(){
	nrf_gpio_pin_clear(BUZ_PIN);
}

