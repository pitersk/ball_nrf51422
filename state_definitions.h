/*
 * state_definitions.h
 *
 *  Created on: 17.04.2017
 *      Author: piter
 */

#ifndef STATE_DEFINITIONS_H_
#define STATE_DEFINITIONS_H_

#include "stddef.h"

#include "nrf.h"
#include "nrf_gpio.h"
#include "app_timer.h"
#include "timers.h"
#include "custom_peripheral_service.h"
#include "ble_functions.h"
#include "pwm_functions.h"
#include "twi_functions.h"
#include "sensor_functions.h"
#include "global_defines.h"
#include "buzzer_functions.h"
#include "gauge_functions.h"




extern volatile ball_state_t client_next_state_request;
extern volatile ball_state_t current_state;

extern volatile uint8_t client_next_sensor_data_interval_ms_request;
extern volatile uint8_t client_next_battery_data_interval_s_request;


ball_state_t do_state_function(ball_state_t state);

ball_state_t do_state_init( );
ball_state_t do_state_play();
ball_state_t do_state_search( );
ball_state_t do_state_charge( );
ball_state_t do_state_error( );
ball_state_t do_state_change_interval();
ball_state_t do_state_trigger_ball();




#endif /* STATE_DEFINITIONS_H_ */
