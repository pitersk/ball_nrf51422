/*
 * timers.c
 *
 *  Created on: 19.04.2017
 *      Author: piter
 */

#include "timers.h"
#include "led_functions.h"

APP_TIMER_DEF(led_timer);
APP_TIMER_DEF(sensor_timer);
APP_TIMER_DEF(battery_timer);

uint8_t sensor_data_interval_ms = SENSOR_TIMER_INITIAL_INTERVAL_MS;
uint8_t battery_data_interval_s = BATTERY_TIMER_INITIAL_INTERVAL_S;

void timersInit(){
	APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

	uint32_t err_code;

	err_code = app_timer_create(&sensor_timer, APP_TIMER_MODE_REPEATED, sensorTimerHandler);
	APP_ERROR_CHECK(err_code);

	err_code = app_timer_create(&led_timer, APP_TIMER_MODE_REPEATED, ledTimerHandler);
	APP_ERROR_CHECK(err_code);

	err_code = app_timer_create(&battery_timer, APP_TIMER_MODE_REPEATED, batteryTimerHandler);
	APP_ERROR_CHECK(err_code);

}

void startLEDTimer(uint16_t timerPeriodms) {
	uint32_t err_code;
	uint32_t time_ticks;
	time_ticks = APP_TIMER_TICKS(timerPeriodms, APP_TIMER_PRESCALER);
	err_code = app_timer_start(led_timer, time_ticks, NULL);
	APP_ERROR_CHECK(err_code);
}

void startSensorTimer( uint16_t timerPeriodms) {
	uint32_t err_code;
	uint32_t time_ticks;
	time_ticks = APP_TIMER_TICKS(timerPeriodms, APP_TIMER_PRESCALER);
	err_code = app_timer_start(sensor_timer, time_ticks, NULL);
	APP_ERROR_CHECK(err_code);
}

void startBatteryTimer( uint16_t timerPeriods) {
	uint32_t err_code;
	uint32_t time_ticks;
	time_ticks = APP_TIMER_TICKS(timerPeriods*1000, APP_TIMER_PRESCALER);
	err_code = app_timer_start(battery_timer, time_ticks, NULL);
	APP_ERROR_CHECK(err_code);
}

void stopLEDTimer(){
	uint32_t err_code = app_timer_stop(led_timer);
	APP_ERROR_CHECK(err_code);
}

void stopSensorTimer(){
	uint32_t err_code = app_timer_stop(sensor_timer);
	APP_ERROR_CHECK(err_code);
}

void stopBatteryTimer(){
	uint32_t err_code = app_timer_stop(battery_timer);
	APP_ERROR_CHECK(err_code);
}

