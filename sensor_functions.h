/*
 * sensor_functions.h
 *
 *  Created on: 27.04.2017
 *      Author: piter
 */

#ifndef SENSOR_FUNCTIONS_H_
#define SENSOR_FUNCTIONS_H_


#include "BMG160_driver.h"
#include "BMC150_driver.h"
#include "timers.h"


typedef struct {
	int16_t bmg160X;
	int16_t bmg160Y;
	int16_t bmg160Z;

	int16_t bmc_acc_150X;
	int16_t bmc_acc_150Y;
	int16_t bmc_acc_150Z;

	int16_t bmc_mag_150X;
	int16_t bmc_mag_150Y;
	int16_t bmc_mag_150Z;

} sensor_data_t;

void initSensors();
void sensorTimerHandler();
void getSensorData();



#endif /* SENSOR_FUNCTIONS_H_ */
