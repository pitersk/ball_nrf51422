/*
 * BMC150_driver.h
 *
 *  Created on: 30.04.2017
 *      Author: piter
 */

#ifndef BMC150_DRIVER_H_
#define BMC150_DRIVER_H_

#include <stdint.h>
#include "nordic_common.h"
#include "twi_functions.h"

#define BMC150_ACC_I2C_SLAVE_ADDRESS	0x10
#define BMC150_MAG_I2C_SLAVE_ADDRESS	0x12

/*******************************************/
/**\name	Accelerometer DATA REGISTER       */
/*******************************************/
#define BMC150_ACC_X_LSB_ADDR                     (0x02)
/**<        Address of X axis Rate LSB Register       */
#define BMC150_ACC_X_MSB_ADDR                     (0x03)
/**<        Address of X axis Rate MSB Register       */
#define BMC150_ACC_Y_LSB_ADDR                     (0x04)
/**<        Address of Y axis Rate LSB Register       */
#define BMC150_ACC_Y_MSB_ADDR                     (0x05)
/**<        Address of Y axis Rate MSB Register       */
#define BMC150_ACC_Z_LSB_ADDR                     (0x06)
/**<        Address of Z axis Rate LSB Register       */
#define BMC150_ACC_Z_MSB_ADDR                     (0x07)
/**<        Address of Z axis Rate MSB Register       */

/*******************************************/
/**\name	Magnetometer DATA REGISTER       */
/*******************************************/
#define BMC150_MAG_X_LSB_ADDR                     (0x42)
/**<        Address of X axis Rate LSB Register       */
#define BMC150_MAG_X_MSB_ADDR                     (0x43)
/**<        Address of X axis Rate MSB Register       */
#define BMC150_MAG_Y_LSB_ADDR                     (0x44)
/**<        Address of Y axis Rate LSB Register       */
#define BMC150_MAG_Y_MSB_ADDR                     (0x45)
/**<        Address of Y axis Rate MSB Register       */
#define BMC150_MAG_Z_LSB_ADDR                     (0x46)
/**<        Address of Z axis Rate LSB Register       */
#define BMC150_MAG_Z_MSB_ADDR                     (0x47)
/**<        Address of Z axis Rate MSB Register       */

/********************************************/
/**\name	POWER MODE DEFINITIONS      */
/********************************************/
/* Control Registers */
#define BMC150_POWER_CONTROL               (0x4B)
#define BMC150_CONTROL                     (0x4C)

#define BMC150_XYZ_DATA_LENGTH		(6)

#define BMC150_RETURN_TYPE		int8_t


void bmc150_get_data(int16_t *acc_x_val, int16_t *acc_y_val, int16_t *acc_z_val,
		int16_t *mag_x_val, int16_t *mag_y_val, int16_t *mag_z_val);

void bmc150_init_mag_normal_mode();

#endif /* BMC150_DRIVER_H_ */
