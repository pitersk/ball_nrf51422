/*
 * twi_functions.c
 *
 *  Created on: 18.04.2017
 *      Author: piter
 */

#include "twi_functions.h"

/* TWI instance. */
//const nrf_drv_twi_t twi_bq27421_instance = NRF_DRV_TWI_INSTANCE(1);
const nrf_drv_twi_t twi_sensors_instance = NRF_DRV_TWI_INSTANCE(1);

void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_sensors_config = {
       .scl                = I2C_SENSORS_SCL_PIN,
       .sda                = I2C_SENSORS_SDA_PIN,
       .frequency          = NRF_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_LOW
    };

    err_code = nrf_drv_twi_init(&twi_sensors_instance, &twi_sensors_config, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&twi_sensors_instance);
}

void twi_read_register(uint8_t slave_address, uint8_t register_address, uint8_t *read_data, uint8_t data_length) {
	// Send register address
	nrf_drv_twi_tx(&twi_sensors_instance, slave_address, &register_address, 1, true);
	// Receive data
	nrf_drv_twi_rx(&twi_sensors_instance, slave_address, read_data, data_length, false);
}

void twi_write_register(uint8_t slave_address, uint8_t register_address, uint8_t *data, uint8_t data_length){
	uint8_t data_packet[data_length + 1];
	data_packet[0] = register_address;
	memcpy(data_packet+1,data, data_length);
	nrf_drv_twi_tx(&twi_sensors_instance, slave_address, data_packet, data_length +1, false);
}
