
#include "custom_peripheral_service.h"
#include <stdlib.h>
#include <string.h>
#include "app_error.h"
#include "ble_gatts.h"
#include "nordic_common.h"
#include "ble_srv_common.h"
#include "app_util.h"

static uint16_t                 service_handle;
static ble_gatts_char_handles_t sensor_char_handles;
static ble_gatts_char_handles_t ball_state_char_handles;
static ble_gatts_char_handles_t battery_char_handles;
static ble_gatts_char_handles_t battery_interval_char_handles;
static ble_gatts_char_handles_t sensor_interval_char_handles;
static ble_gatts_char_handles_t	led_color_char_handles;


/* Initializes characteristics in the service, sets initial values */
uint32_t custom_service_init(void)
{
    uint32_t   err_code;
    ble_uuid_t service_uuid;
    ble_uuid128_t base_uuid = BLE_BASE_UUID;
    service_uuid.uuid = BLE_UUID_BALL_SERVICE;

    err_code = sd_ble_uuid_vs_add(&base_uuid, &service_uuid.type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }


    ble_srv_security_mode_t        dis_attr_md;



    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &service_uuid, &service_handle);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    //*create custom characteristics*/
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_gatts_attr_md_t attr_md;

    uint8_t initial_sensor_char_values[SENSOR_CHARACTERISTIC_SIZE];
    uint8_t initial_ball_state_char_values[BALL_STATE_CHARACTERISTIC_SIZE];
    uint8_t initial_battery_char_values[BATTERY_CHARACTERISTIC_SIZE];
    uint8_t initial_battery_int_char_values[BATTERY_INTERVAL_CHAR_SIZE];
    uint8_t initial_sensor_int_values[SENSOR_INTERVAL_CHAR_SIZE];

    /* Set initial values of characteristics to 0 */
    memset(initial_sensor_char_values, 0, sizeof(initial_sensor_char_values));
    memset(initial_ball_state_char_values, 0, sizeof(initial_ball_state_char_values));
    memset(initial_battery_char_values, 0, sizeof(initial_battery_char_values));
    memset(initial_battery_int_char_values, 0, sizeof(initial_battery_int_char_values));
    memset(initial_sensor_int_values, 0, sizeof(initial_sensor_int_values));

    /*Characteristic 1, for reading sensor data*/
    memset(&char_md, 0, sizeof(char_md));

    char sensorDataUserDescription[] = "Sensor Data";

    char_md.char_props.read		= 1;
    char_md.char_props.notify	= 1;
    char_md.p_char_user_desc = (uint8_t*) sensorDataUserDescription;
    char_md.char_user_desc_size = sizeof(sensorDataUserDescription);
    char_md.char_user_desc_max_size = sizeof(sensorDataUserDescription);
    char_md.p_char_pf        = NULL;
    char_md.p_user_desc_md   = NULL;
    char_md.p_cccd_md        = NULL;
    char_md.p_sccd_md        = NULL;

    BLE_UUID_BLE_ASSIGN(service_uuid, BLE_UUID_SENSOR_CHAR);

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.write_perm);
    attr_md.read_perm  = dis_attr_md.read_perm;
    attr_md.write_perm = dis_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &service_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = SENSOR_CHARACTERISTIC_SIZE;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = SENSOR_CHARACTERISTIC_SIZE;
    attr_char_value.p_value   = initial_sensor_char_values;

    err_code =  sd_ble_gatts_characteristic_add(service_handle, &char_md, &attr_char_value, &sensor_char_handles);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    /*Characteristic 2, for setting state of the ball */
    memset(&char_md, 0, sizeof(char_md));

    char ledsCharUserDescription[] = "Ball State";

    char_md.char_props.read  = 1;
    char_md.char_props.write  = 1;
    char_md.char_props.write_wo_resp  = 1;
    char_md.p_char_user_desc = (uint8_t*)ledsCharUserDescription;
    char_md.char_user_desc_size = sizeof(ledsCharUserDescription);
    char_md.char_user_desc_max_size = sizeof(ledsCharUserDescription);
    char_md.p_char_pf        = NULL;
    char_md.p_user_desc_md   = NULL;
    char_md.p_cccd_md        = NULL;
    char_md.p_sccd_md        = NULL;

    BLE_UUID_BLE_ASSIGN(service_uuid, BLE_UUID_BALL_STATE_CHAR);

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.write_perm);
    attr_md.read_perm  = dis_attr_md.read_perm;
    attr_md.write_perm = dis_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &service_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = BALL_STATE_CHARACTERISTIC_SIZE;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BALL_STATE_CHARACTERISTIC_SIZE;
    attr_char_value.p_value   = initial_sensor_char_values;

	ble_gatts_attr_md_t cccd_md;
	memset(&cccd_md, 0, sizeof(cccd_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
	cccd_md.vloc                = BLE_GATTS_VLOC_STACK;
	char_md.p_cccd_md           = &cccd_md;


    err_code =  sd_ble_gatts_characteristic_add(service_handle, &char_md, &attr_char_value, &ball_state_char_handles);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    /*Characteristic 3, device's battery state in % */
    memset(&char_md, 0, sizeof(char_md));

    char buzzerCharUserDescription[] = "Battery Level";

    char_md.char_props.read  = 1;
    char_md.char_props.write  = 0;
    char_md.char_props.notify = 1;
    char_md.p_char_user_desc = (uint8_t*)buzzerCharUserDescription;
    char_md.char_user_desc_size = sizeof(buzzerCharUserDescription);
    char_md.char_user_desc_max_size = sizeof(buzzerCharUserDescription);
    char_md.p_char_pf        = NULL;
    char_md.p_user_desc_md   = NULL;
    char_md.p_cccd_md        = NULL;
    char_md.p_sccd_md        = NULL;

    BLE_UUID_BLE_ASSIGN(service_uuid, BLE_UUID_BATTERY_CHAR);

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.write_perm);
    attr_md.read_perm  = dis_attr_md.read_perm;
    attr_md.write_perm = dis_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &service_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = BATTERY_CHARACTERISTIC_SIZE;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BATTERY_CHARACTERISTIC_SIZE;
    attr_char_value.p_value   = initial_sensor_char_values;

	memset(&cccd_md, 0, sizeof(cccd_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
	cccd_md.vloc                = BLE_GATTS_VLOC_STACK;
	char_md.p_cccd_md           = &cccd_md;


    err_code =  sd_ble_gatts_characteristic_add(service_handle, &char_md, &attr_char_value, &battery_char_handles);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    /*Characteristic 4, device's battery message interval */

    char batteryIntCharUserDescription[] = "Battery Interval";

    char_md.char_props.read  = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc = (uint8_t*)batteryIntCharUserDescription;
    char_md.char_user_desc_size = sizeof(batteryIntCharUserDescription);
    char_md.char_user_desc_max_size = sizeof(batteryIntCharUserDescription);
    char_md.p_char_pf        = NULL;
    char_md.p_user_desc_md   = NULL;
    char_md.p_cccd_md        = NULL;
    char_md.p_sccd_md        = NULL;

    BLE_UUID_BLE_ASSIGN(service_uuid, BLE_UUID_BATTERY_INTERVAL_CHAR);

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.write_perm);
    attr_md.read_perm  = dis_attr_md.read_perm;
    attr_md.write_perm = dis_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &service_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = BATTERY_INTERVAL_CHAR_SIZE;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BATTERY_INTERVAL_CHAR_SIZE;
    attr_char_value.p_value   = initial_battery_int_char_values;

	memset(&cccd_md, 0, sizeof(cccd_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
	cccd_md.vloc                = BLE_GATTS_VLOC_STACK;
	char_md.p_cccd_md           = &cccd_md;


    err_code =  sd_ble_gatts_characteristic_add(service_handle, &char_md, &attr_char_value, &battery_interval_char_handles);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }


    /*Characteristic 5, device's sensor message interval */

    char sensorIntCharUserDescription[] = "Sensor Interval";

    char_md.char_props.read  = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc = (uint8_t*)sensorIntCharUserDescription;
    char_md.char_user_desc_size = sizeof(sensorIntCharUserDescription);
    char_md.char_user_desc_max_size = sizeof(sensorIntCharUserDescription);
    char_md.p_char_pf        = NULL;
    char_md.p_user_desc_md   = NULL;
    char_md.p_cccd_md        = NULL;
    char_md.p_sccd_md        = NULL;

    BLE_UUID_BLE_ASSIGN(service_uuid, BLE_UUID_SENSOR_INTERVAL_CHAR);

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.write_perm);
    attr_md.read_perm  = dis_attr_md.read_perm;
    attr_md.write_perm = dis_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &service_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = SENSOR_INTERVAL_CHAR_SIZE;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = SENSOR_INTERVAL_CHAR_SIZE;
    attr_char_value.p_value   = initial_sensor_int_values;

	memset(&cccd_md, 0, sizeof(cccd_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
	cccd_md.vloc                = BLE_GATTS_VLOC_STACK;
	char_md.p_cccd_md           = &cccd_md;


    err_code =  sd_ble_gatts_characteristic_add(service_handle, &char_md, &attr_char_value, &sensor_interval_char_handles);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    /*Characteristic 6, device LED color characteristic*/
    uint8_t initial_led_color_values[LED_COLOR_CHAR_SIZE]= {0,0,0};
    char ledColorCharUserDescription[] = "LED Color";

    char_md.char_props.read  = 0;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc = (uint8_t*)ledColorCharUserDescription;
    char_md.char_user_desc_size = sizeof(ledColorCharUserDescription);
    char_md.char_user_desc_max_size = sizeof(ledColorCharUserDescription);
    char_md.p_char_pf        = NULL;
    char_md.p_user_desc_md   = NULL;
    char_md.p_cccd_md        = NULL;
    char_md.p_sccd_md        = NULL;

    BLE_UUID_BLE_ASSIGN(service_uuid, BLE_UUID_LED_COLOR_CHAR);

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_attr_md.write_perm);
    attr_md.read_perm  = dis_attr_md.read_perm;
    attr_md.write_perm = dis_attr_md.write_perm;
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &service_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = LED_COLOR_CHAR_SIZE;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = LED_COLOR_CHAR_SIZE;
    attr_char_value.p_value   = initial_led_color_values;

	memset(&cccd_md, 0, sizeof(cccd_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
	cccd_md.vloc                = BLE_GATTS_VLOC_STACK;
	char_md.p_cccd_md           = &cccd_md;


    err_code =  sd_ble_gatts_characteristic_add(service_handle, &char_md, &attr_char_value, &led_color_char_handles);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    return NRF_SUCCESS;
}

uint32_t service_update_sensor_char(uint16_t conn_handle,uint8_t *new_data)
{
    uint32_t err_code = NRF_SUCCESS;
    ble_gatts_value_t gatts_value;
    memset(&gatts_value, 0, sizeof(gatts_value));
    gatts_value.len     = SENSOR_CHARACTERISTIC_SIZE;
    gatts_value.offset  = 0;
    gatts_value.p_value = new_data;
    if(conn_handle!=BLE_CONN_HANDLE_INVALID)
    {
    	uint16_t    len = SENSOR_CHARACTERISTIC_SIZE;
    	ble_gatts_hvx_params_t hvx_params;
    	memset(&hvx_params, 0, sizeof(hvx_params));

    	hvx_params.handle = sensor_char_handles.value_handle;
    	hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
    	hvx_params.offset = 0;
    	hvx_params.p_len  = &len;
    	hvx_params.p_data = (uint8_t*)new_data;

    	err_code = sd_ble_gatts_hvx(conn_handle, &hvx_params);

       /* err_code = sd_ble_gatts_value_set(conn_handle,
                                          char2_handles.value_handle,
                                        &gatts_value); */
    }
    return err_code;
}

uint32_t service_update_battery_char(uint16_t conn_handle,uint8_t *new_data)
{
    uint32_t err_code = NRF_SUCCESS;
    ble_gatts_value_t gatts_value;
    memset(&gatts_value, 0, sizeof(gatts_value));
    gatts_value.len     = BATTERY_CHARACTERISTIC_SIZE;
    gatts_value.offset  = 0;
    gatts_value.p_value = new_data;
    if(conn_handle!=BLE_CONN_HANDLE_INVALID)
    {
    	uint16_t    len = BATTERY_CHARACTERISTIC_SIZE;
    	ble_gatts_hvx_params_t hvx_params;
    	memset(&hvx_params, 0, sizeof(hvx_params));

    	hvx_params.handle = battery_char_handles.value_handle;
    	hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
    	hvx_params.offset = 0;
    	hvx_params.p_len  = &len;
    	hvx_params.p_data = (uint8_t*)new_data;

    	err_code = sd_ble_gatts_hvx(conn_handle, &hvx_params);

       /* err_code = sd_ble_gatts_value_set(conn_handle,
                                          char2_handles.value_handle,
                                        &gatts_value); */
    }
    return err_code;
}
