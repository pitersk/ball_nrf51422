/*
 * uart_functions.h
 *
 *  Created on: 11 maj 2016
 *      Author: Piotrek
 */
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "app_uart.h"
#include "app_error.h"
#include "nrf_delay.h"
#include "nrf.h"
#include "bsp.h"

#ifndef UART_FUNCTIONS_H_
#define UART_FUNCTIONS_H_



#define MAX_TEST_DATA_BYTES     (15U)                /**< max number of test bytes to be used for tx and rx. */
#define UART_TX_BUF_SIZE 256                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 1                           /**< UART RX buffer size. */

#define UART_TX_PIN 10
#define UART_RX_PIN 8

void uart_error_handle(app_uart_evt_t * p_event);

void uart_init();

void uart_test();

void uart_send_string(char* message);

#endif /* UART_FUNCTIONS_H_ */
