/*
 * gauge_functions.c
 *
 *  Created on: 15 cze 2017
 *      Author: piter
 */
#include "gauge_functions.h"
#include "twi_functions.h"
#include "custom_peripheral_service.h"
#include "ble_functions.h"


void batteryTimerHandler(){
	uint32_t err_code;
	uint8_t new_data[BATTERY_CHARACTERISTIC_SIZE];
	unsigned int battery_data;
	battery_data = gauge_cmd_read(0, CMD_SOC);
	err_code = service_update_battery_char(m_conn_handle, &battery_data);
	APP_ERROR_CHECK(err_code);
}

// nI2C = handle to your I2C driver (if required)
// nRegister = gauge register
// pData = pointer to data block which will hold the data from the gauge
// nLength = length
void i2c_read(int nI2C, unsigned char nRegister, unsigned char *pData, unsigned char nLength)
{
	// implement your I2C read function here. Make sure you follow the timing requirements from the datasheet
	twi_read_register(GAUGE_DEVICE_ADDRESS, nRegister, pData, nLength);
}

// nI2C = handle to your I2C driver (if required)
// nRegister = gauge register
// pData = pointer to data block which holds the data for the gauge
// nLength = length
void i2c_write(int nI2C, unsigned char nRegister, unsigned char *pData, unsigned char nLength)
{
	// implement your I2C write function here. Make sure you follow the timing requirements from the datasheet
	twi_write_register(GAUGE_DEVICE_ADDRESS, nRegister, pData, nLength);
}

// issue a control command (nSubCmd = sub command) to the gauge
unsigned int gauge_control(int nI2C, unsigned int nSubCmd)
{
	unsigned int nResult = 0;

	char pData[2];

	pData[0] = nSubCmd & 0xFF;
	pData[1] = (nSubCmd >> 8) & 0xFF;

	i2c_write(nI2C, 0x00, pData, 2); // issue control and sub command

	i2c_read(nI2C, 0x00, pData, 2); // read data

	nResult = (pData[1] << 8) | pData[0];

	return nResult;
}

// read 2 bytes from the gauge (nCmd = command)
unsigned int gauge_cmd_read(int nI2C, unsigned char nCmd)
{
	unsigned char pData[2];

	i2c_read(nI2C, nCmd, pData, 2);

	return (pData[1] << 8) | pData[0];
}

// write 2 bytes to the gauge (nCmd = command, nData = 16 bit parameter)
unsigned int gauge_cmd_write(int nI2C, unsigned char nCmd, unsigned int nData)
{
	unsigned char pData[2];

	pData[0] = nData & 0xFF;
	pData[1] = (nData >> 8) & 0xFF;

	i2c_write(nI2C, nCmd, pData, 2);

	return (pData[1] << 8) | pData[0];
}

#define MAX_ATTEMPTS 5
// exit CFG_UPDATE mode with one of the supported exit commands (e.g. SOFT_RESET)
int gauge_exit(int nI2C, unsigned int nCmd)
{
	unsigned int nFlags;
	int nAttempts = 0;
	gauge_control(nI2C, nCmd);

	do
	{
		nFlags = gauge_cmd_read(nI2C, CMD_FLAGS);
		if (nFlags & CFGUPD) nrf_delay_us(500000);
	} while ((nFlags & CFGUPD) && (nAttempts++ < MAX_ATTEMPTS));

	return (nAttempts < MAX_ATTEMPTS);
}

// enter CFG_UPDATE mode
int gauge_cfg_update(int nI2C)
{
	unsigned int nFlags;
	int nAttempts = 0;
	gauge_control(nI2C, SET_CFGUPDATE);

	do
	{
		nFlags = gauge_cmd_read(nI2C, CMD_FLAGS);
		if (!(nFlags & CFGUPD)) nrf_delay_us(500000);
	} while (!(nFlags & CFGUPD) && (nAttempts++ < MAX_ATTEMPTS));

	return (nAttempts < MAX_ATTEMPTS);
}

// read a data class.
// nDataClass = data class number
// pData = raw data (for the whole data class)
// nLength = length of the whole data class
int gauge_read_data_class(int nI2C, unsigned char nDataClass, unsigned char *pData, unsigned char nLength)
{
	unsigned char nRemainder = nLength;
	unsigned int nOffset = 0;
	unsigned char nDataBlock = 0x00;
	unsigned int nData;

	if (nLength < 1) return 0;

	do
	{

		nLength = nRemainder;
		if (nLength > 32)
		{
			nRemainder = nLength - 32;
			nLength = 32;
		}
		else nRemainder = 0;

		nData = (nDataBlock << 8) | nDataClass;
		gauge_cmd_write(nI2C, CMD_DATA_CLASS, nData);

		i2c_read(nI2C, CMD_BLOCK_DATA, pData, nLength);

		pData += nLength;
		nDataBlock++;
	} while (nRemainder > 0);

	return nLength;
}

unsigned char check_sum(unsigned char *pData, unsigned char nLength)
{
	unsigned char nSum = 0x00;
	unsigned char n;

	for (n = 0; n < nLength; n++)
		nSum += pData[n];

	nSum = 0xFF - nSum;

	return nSum;
}

// write a data class.
// nDataClass = data class number
// pData = raw data (for the whole data class)
// nLength = length of the whole data class
int gauge_write_data_class(int nI2C, unsigned char nDataClass, unsigned char *pData, unsigned char nLength)
{
	unsigned char nRemainder = nLength;
	unsigned int nOffset = 0;
	unsigned char pCheckSum[2] = {0x00, 0x00};
	unsigned int nData;
	unsigned char nDataBlock = 0x00;

	if (nLength < 1) return 0;

	do
	{
		nLength = nRemainder;
		if (nLength > 32)
		{
			nRemainder = nLength - 32;
			nLength = 32;
		}
		else nRemainder = 0;

		nData = (nDataBlock << 8) | nDataClass;
		gauge_cmd_write(nI2C, CMD_DATA_CLASS, nData);

		i2c_write(nI2C, CMD_BLOCK_DATA, pData, nLength);
		pCheckSum[0] = check_sum(pData, nLength);
		i2c_write(nI2C, CMD_CHECK_SUM, pCheckSum, 1);

		nrf_delay_us(10000);

		gauge_cmd_write(nI2C, CMD_DATA_CLASS, nData);
		i2c_read(nI2C, CMD_CHECK_SUM, pCheckSum + 1, 1);
		if (pCheckSum[0] != pCheckSum[1])
			printf("gauge_write_data_class(): CheckSum mismatch 0x%02X vs. 0x%02X\n\r", pCheckSum[0], pCheckSum[1]);

		pData += nLength;
		nDataBlock++;
	} while (nRemainder > 0);

	return nLength;
}

void print_data(unsigned char *pData, unsigned int nLength)
{
	unsigned int n;

	for (n = 0; n < nLength; n++)
	{
		printf("%02X ", pData[n]);
		if (!((n + 1) % 16)) printf("\n\r");
	}

	printf("\n\r");
}

void configFuelGauge(){
	unsigned char pData[DC_STATE_LENGTH];
	unsigned int nFlags;
	int nAttempts = 0;
	int nI2C = 0;
	gauge_control(nI2C, SOFT_RESET);
	do
	{
		nFlags = gauge_cmd_read(nI2C, CMD_FLAGS);
		if (nFlags & ITPOR) nrf_delay_us(500000); //nrf_delay_us sleeps for n microseconds.
	} while ((nFlags & ITPOR) && (nAttempts++ < MAX_ATTEMPTS));

	//read data class DC_STATE:
	gauge_read_data_class(nI2C, DC_STATE, pData, DC_STATE_LENGTH);
	printf("Data Class 'State' (0x52):\n\r");
	print_data(pData, DC_STATE_LENGTH);

	// this was for bq2742x - change offsets for your gauge
	pData[10] = (DESIGN_CAPACITY & 0xFF00) >> 8;
	pData[11] = DESIGN_CAPACITY & 0xFF;

	pData[12] = ((unsigned int)DESIGN_ENERGY & 0xFF00) >> 8;
	pData[13] = (unsigned int)DESIGN_ENERGY & 0xFF;

	pData[16] = (TERMINATE_VOLTAGE & 0xFF00) >> 8;
	pData[17] = TERMINATE_VOLTAGE & 0xFF;

	pData[27] = ((unsigned int)TAPER_RATE & 0xFF00) >> 8;
	pData[28] = (unsigned int)TAPER_RATE & 0xFF;

	//write data class DC_STATE:
	gauge_cfg_update(nI2C);
	gauge_write_data_class(nI2C, DC_STATE, pData, DC_STATE_LENGTH);
	gauge_exit(nI2C, SOFT_RESET);
}

/*
Example configuration:

	#define MAX_ATTEMPTS	5

	#define DESIGN_CAPACITY		3210										//[mAh]
	#define NOMINAL_VOLTAGE		3.7											//[V]
	#define DESIGN_ENERGY		(DESIGN_CAPACITY * NOMINAL_VOLTAGE)			//[mWh]
	#define TERMINATE_VOLTAGE	3000										//[mV]
	#define TAPER_CURRENT		115											//[mA]
	#define TAPER_RATE			(DESIGN_CAPACITY / (0.1 * TAPER_CURRENT))	//[0.1h]

	// open your I2C communications channel and store a handle in nI2C (if required)

	unsigned char pData[DC_STATE_LENGTH];
	unsigned int nFlags;
	int nAttempts = 0;

	gauge_control(nI2C, SOFT_RESET);
	do
	{
		nFlags = gauge_cmd_read(nI2C, CMD_FLAGS);
		if (nFlags & ITPOR) nrf_delay_us(500000); //nrf_delay_us sleeps for n microseconds.
	} while ((nFlags & ITPOR) && (nAttempts++ < MAX_ATTEMPTS));

	//read data class DC_STATE:
	gauge_read_data_class(nI2C, DC_STATE, pData, DC_STATE_LENGTH);
	printf("Data Class 'State' (0x52):\n\r");
	print_data(pData, DC_STATE_LENGTH);

	// this was for bq2742x - change offsets for your gauge
	pData[10] = (DESIGN_CAPACITY & 0xFF00) >> 8;
	pData[11] = DESIGN_CAPACITY & 0xFF;

	pData[12] = (DESIGN_ENERGY & 0xFF00) >> 8;
	pData[13] = DESIGN_ENERGY & 0xFF;

	pData[16] = (TERMINATE_VOLTAGE & 0xFF00) >> 8;
	pData[17] = TERMINATE_VOLTAGE & 0xFF;

	pData[27] = (TAPER_RATE & 0xFF00) >> 8;
	pData[28] = TAPER_RATE & 0xFF;

	//write data class DC_STATE:
	gauge_cfg_update(nI2C);
	gauge_write_data_class(nI2C, DC_STATE, pData, DC_STATE_LENGTH);
	gauge_exit(nI2C, SOFT_RESET);

	//read several gauge registers
	nResult = gauge_cmd_read(nI2C, CMD_TEMP);
	printf("TEMPERATURE = 0x%04X\n\r", nResult);

	nResult = gauge_cmd_read(nI2C, CMD_VOLT);
	printf("VOLTAGE = 0x%04X\n\r", nResult);

	nResult = gauge_cmd_read(nI2C, CMD_SOC);
	printf("SOC = 0x%04X\n\r", nResult);

*/



