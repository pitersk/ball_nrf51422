/*
 * pwm_functions.h
 *
 *  Created on: 16.04.2017
 *      Author: piter
 */

#ifndef PWM_FUNCTIONS_H_
#define PWM_FUNCTIONS_H_


#include "nrf.h"
#include "nrf_drv_config.h"
#include "app_error.h"
#include "app_pwm.h"

#define PWM_R_PIN	11
#define PWM_B_PIN 	10
#define PWM_G_PIN	12


#define PWM_LED_PERIOD_US 100
#define PWM_BUZ_PERIOD_US 1000000


#define PWM_R_CHANNEL 0
#define PWM_G_CHANNEL 1
#define PWM_B_CHANNEL 0
#define PWM_BUZ_CHANNEL 0


extern const app_pwm_t PWM_R_AND_G_INSTANCE;
extern const app_pwm_t PWM_B_INSTANCE;
extern const app_pwm_t PWM_BUZ_INSTANCE;


void init_PWM();
void enable_PWM();

#endif /* PWM_FUNCTIONS_H_ */
