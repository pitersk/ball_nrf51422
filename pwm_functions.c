/*
 * pwm_functions.c
 *
 *  Created on: 16.04.2017
 *      Author: piter
 */

#include "pwm_functions.h"


APP_PWM_INSTANCE(PWM_R_AND_G_INSTANCE,2);                   // Create the instance "PWM_R_AND_G_INSTANCE" using TIMER0.
APP_PWM_INSTANCE(PWM_B_INSTANCE,1);                   // Create the instance "PWM_B" using TIMER1.
//APP_PWM_INSTANCE(PWM_BUZ_INSTANCE,0);                   // Create the instance "PWM_BUZ" using TIMER2.


void init_PWM(){

	uint32_t err_code;

	// For this to compile, modify libraries/pwm/app_pwm.c line with function pwm_calculate_timer_frequency
	// to __STATIC_INLINE nrf_timer_frequency_t pwm_calculate_timer_frequency(uint32_t period_us)
	app_pwm_config_t pwm_r_and_g_cfg = APP_PWM_DEFAULT_CONFIG_2CH(PWM_LED_PERIOD_US,PWM_R_PIN,PWM_G_PIN);
	app_pwm_config_t pwm_b_cfg = APP_PWM_DEFAULT_CONFIG_1CH(PWM_LED_PERIOD_US,PWM_B_PIN);
	//app_pwm_config_t pwm_buz_cfg = APP_PWM_DEFAULT_CONFIG_1CH(PWM_BUZ_PERIOD_US,PWM_BUZ_PIN);

	pwm_r_and_g_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;
	pwm_r_and_g_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;
	pwm_b_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;
//	pwm_buz_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;

	err_code = app_pwm_init(&PWM_R_AND_G_INSTANCE, &pwm_r_and_g_cfg, NULL);
	APP_ERROR_CHECK(err_code);
	err_code = app_pwm_init(&PWM_B_INSTANCE, &pwm_b_cfg, NULL);
	APP_ERROR_CHECK(err_code);
//	err_code = app_pwm_init(&PWM_BUZ_INSTANCE, &pwm_buz_cfg, NULL);
//	APP_ERROR_CHECK(err_code);


}

void enable_PWM(){
	app_pwm_enable(&PWM_R_AND_G_INSTANCE);
	app_pwm_enable(&PWM_B_INSTANCE);
//	app_pwm_enable(&PWM_BUZ_INSTANCE);
}
