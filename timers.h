/*
 * timers.h
 *
 *  Created on: 19.04.2017
 *      Author: piter
 */

#ifndef TIMERS_H_
#define TIMERS_H_

#include <stdbool.h>
#include "app_timer.h"
#include "nrf_drv_clock.h"
#include "sensor_functions.h"
#include "gauge_functions.h"

// General application timer settings.
#define APP_TIMER_PRESCALER             0    // Value of the RTC1 PRESCALER register.
#define APP_TIMER_OP_QUEUE_SIZE         3     // Size of timer operation queues.

#define SENSOR_TIMER_INITIAL_INTERVAL_MS 10
#define BATTERY_TIMER_INITIAL_INTERVAL_S 2

extern uint8_t sensor_data_interval_ms;
extern uint8_t battery_data_interval_s;


void timersInit();

void startLEDTimer( uint16_t timerPeriodms);
void startSensorTimer( uint16_t timerPeriodms);
void startBatteryTimer( uint16_t timerPeriods);
void stopLEDTimer();
void stopSensorTimer();
void stopBatteryTimer();





#endif /* TIMERS_H_ */
