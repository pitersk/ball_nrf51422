#ifndef CUSTOM_PERIPHERAL_SERVICE_H__
#define CUSTOM_PERIPHERAL_SERVICE_H__

#include <stdint.h>
#include <stdio.h>
#include "ble_srv_common.h"


// Our BASE UUID 310Exxxx-8C06-43CB-9C74-A19B5DE547C1
#define BLE_BASE_UUID			{0xC1,0x47,0xE5,0x5D,0x9B,0xA1,0x74,0x9C,0xCB,0x43,0x06,0x8C,0x00,0x00,0x0E,0x31}
#define BLE_UUID_BALL_SERVICE             0xABCD
#define BLE_UUID_SENSOR_CHAR              0x0001
#define BLE_UUID_BALL_STATE_CHAR          0x0002
#define BLE_UUID_BATTERY_CHAR             0x0003
#define BLE_UUID_BATTERY_INTERVAL_CHAR    0x0004
#define BLE_UUID_SENSOR_INTERVAL_CHAR     0x0005
#define BLE_UUID_LED_COLOR_CHAR			  0x0006


#define SENSOR_CHARACTERISTIC_SIZE 			  18
#define BALL_STATE_CHARACTERISTIC_SIZE		   1
#define BATTERY_CHARACTERISTIC_SIZE			   1
#define BATTERY_INTERVAL_CHAR_SIZE			   1
#define SENSOR_INTERVAL_CHAR_SIZE			   1
#define LED_COLOR_CHAR_SIZE					   3

uint32_t custom_service_init(void);
uint32_t service_update_sensor_char(uint16_t conn_handle,uint8_t *new_data);
uint32_t service_update_battery_char(uint16_t conn_handle,uint8_t *new_data);

#endif
