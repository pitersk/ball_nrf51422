/*
 * buzzer_functions.h
 *
 *  Created on: 4 maj 2017
 *      Author: piter
 */

#ifndef BUZZER_FUNCTIONS_H_
#define BUZZER_FUNCTIONS_H_

#include "nrf.h"
#include "nrf_gpio.h"


#define  BUZ_PIN	25

void buzzerInit();
void buzzerOn();
void buzzerOff();

#endif /* BUZZER_FUNCTIONS_H_ */
