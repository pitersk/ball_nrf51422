

#include "nrf.h"
#include "app_error.h"
#include "nrf_drv_config.h"
#include "nrf_gpio.h"
#include "nrf_drv_spi.h"

#include "global_defines.h"
#include "state_definitions.h"
#include "led_functions.h"



/*debug functions*/
#ifdef DEBUG
#define DEBUG_PRINT 1
#else
#define DEBUG_PRINT 0
#endif
#define DEBUG_PRINTF(...) do{ if (DEBUG_PRINT) {printf(__VA_ARGS__); printf("\r\n"); }} while(0)

extern volatile ball_state_t client_next_state_request;
extern volatile ball_state_t current_state;

int main(void)
{

	current_state = STATE_INIT;
	client_next_state_request = STATE_INVALID;
	volatile ball_state_t next_state = STATE_INIT;
	volatile ball_state_t last_state = STATE_INVALID;


    while(1)
    {
    	if (last_state != current_state){
        	next_state = do_state_function(current_state);
        	doStateLEDAction(current_state);
    	}
    	// Check whether there was a state change request from client via BLE
    	// Maybe disable interrupts here?
    	if(client_next_state_request != STATE_INVALID){
    		next_state = client_next_state_request;
    		client_next_state_request = STATE_INVALID;
    	}
    	if (!nrf_gpio_pin_read(27)){   // Force charging state when signal from the charger is low
    		next_state = STATE_CHARGE;
    	}
    	last_state = current_state;
    	current_state = next_state;
    }
    return 0;
}






void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name){
    DEBUG_PRINTF("app_error_handle4r\r\nerror_code=%u\r\nline_num=%u\r\nfile_name=%s",error_code,line_num,p_file_name);
}


/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(1, line_num, p_file_name);
}




