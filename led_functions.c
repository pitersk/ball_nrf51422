/*
 * led_functions.c
 *
 *  Created on: 18.04.2017
 *      Author: piter
 */

#include "led_functions.h"

volatile uint8_t client_led_color_request[3] = {0,0,0};

void setLEDColor( uint8_t R, uint8_t G, uint8_t B){
	float scale_factor = 100.0f/255.0f;
	uint8_t R_duty_cycle = R*scale_factor;
	uint8_t G_duty_cycle = G*scale_factor;
	uint8_t B_duty_cycle = B*scale_factor;

	// Wait for PWM cycles to finish and then set new duty cycle
	while (app_pwm_channel_duty_set(&PWM_R_AND_G_INSTANCE, PWM_R_CHANNEL, R_duty_cycle) == NRF_ERROR_BUSY);
	while (app_pwm_channel_duty_set(&PWM_R_AND_G_INSTANCE, PWM_G_CHANNEL, G_duty_cycle) == NRF_ERROR_BUSY);
	while (app_pwm_channel_duty_set(&PWM_B_INSTANCE, PWM_B_CHANNEL, B_duty_cycle) == NRF_ERROR_BUSY);

}

void turnOffLED(){

	// Wait for PWM cycles to finish and then set new duty cycle
	while (app_pwm_channel_duty_set(&PWM_R_AND_G_INSTANCE, PWM_R_CHANNEL, 0) == NRF_ERROR_BUSY);
	while (app_pwm_channel_duty_set(&PWM_R_AND_G_INSTANCE, PWM_G_CHANNEL, 0) == NRF_ERROR_BUSY);
	while (app_pwm_channel_duty_set(&PWM_B_INSTANCE, PWM_B_CHANNEL, 0) == NRF_ERROR_BUSY);
}

void dimmGreenLight()
{
	while (app_pwm_channel_duty_set(&PWM_R_AND_G_INSTANCE, PWM_R_CHANNEL, 0) == NRF_ERROR_BUSY);
	while (app_pwm_channel_duty_set(&PWM_R_AND_G_INSTANCE, PWM_G_CHANNEL, 10) == NRF_ERROR_BUSY);
	while (app_pwm_channel_duty_set(&PWM_B_INSTANCE, PWM_B_CHANNEL, 0) == NRF_ERROR_BUSY);
}


void doStateLEDAction( ball_state_t state ) {


	// Stop the timer when the state changes
	stopLEDTimer();

	switch(state){
	case STATE_INIT:
		initLEDSequence();
		break;
	case STATE_CHARGE:
		dimmGreenLight();
		break;
	case STATE_ERROR:
		errorLEDSequence();
		break;
	case STATE_IDLE:
//		TODO
		break;
	case STATE_PLAY:
		playLEDSequence();
		break;
	case STATE_SEARCH:
		searchLEDSequence();
		break;
	case STATE_STANDBY:
//		TODO
		break;
	case STATE_COLOR_CHANGE:
		colorChangeSequence();
		break;
	case STATE_TRIGGER_BALL:
		triggerBallLEDSequence();
		break;
	default:
		break;
	}
}


// This timer toggles the LEDs with color according to the current system state
void ledTimerHandler(){
	static bool leds_last_state = false;
	ball_state_t state = current_state;
	if( !leds_last_state){
		switch(state){
		case STATE_INIT:
			break;
		case STATE_CHARGE:
			break;
		case STATE_ERROR:
			setLEDColor(255,0,0);
			break;
		case STATE_IDLE:
	//		TODO
			break;
		case STATE_PLAY:
	//		TODO
			break;
		case STATE_SEARCH:
			setLEDColor(255,255,0);
			buzzerOn();
			break;
		case STATE_TRIGGER_BALL:
			buzzerOn();
			setLEDColor(255,255,255);
			break;
		case STATE_STANDBY:
	//		TODO
			break;
		default:
			break;
		}
	}else{
		turnOffLED();
		buzzerOff();
	}

	leds_last_state = !leds_last_state;

}


// TODO Switch delays to timers!
void initLEDSequence(){
	// Two green led impulses to indicate that the device is turned on
	setLEDColor(0,255,0);
	nrf_delay_ms(500);
	setLEDColor(0,0,0);
	nrf_delay_ms(500);
	setLEDColor(0,255,0);
	nrf_delay_ms(500);
	setLEDColor(0,0,0);

}

void searchLEDSequence(){
	// Yellow light impulse every SEARCH_STATE_LED_INTERVAL_MS
	startLEDTimer(SEARCH_STATE_LED_INTERVAL_MS);

}

void errorLEDSequence(){
	// Red light impulse
	startLEDTimer(ERROR_STATE_LED_INTERVAL_MS);
}

void playLEDSequence(){
	// Led Color, no blinking
	setLEDColor(255,0,0);
}

void colorChangeSequence(){
	// Set LED color to color requested from client. No blinking.
	setLEDColor(client_led_color_request[0], client_led_color_request[1], client_led_color_request[2]);
}

void triggerBallLEDSequence(){
	// Yellow light impulse every SEARCH_STATE_LED_INTERVAL_MS
	startLEDTimer(TRIGGER_BALL_STATE_LED_INTERVAL_MS);

}
