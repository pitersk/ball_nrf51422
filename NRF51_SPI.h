/*
 * NRF51_SPI.h
 *
 *  Created on: 10.04.2017
 *      Author: piter
 */

#ifndef NRF51_SPI_H_
#define NRF51_SPI_H_
#include "nrf.h"
#include "nrf_gpio.h"
#include "nrf_drv_spi.h"
#include "nordic_common.h"
#include "nrf_drv_config.h"
#include "app_util_platform.h"

#include <stdbool.h>
#include <string.h>

#define SPI0_USE_EASY_DMA 0
#define SPI0_INSTANCE_INDEX 0
#define SPI0_ENABLED 1

#define SPIM0_SCK_PIN 14
#define SPIM0_MOSI_PIN 15
#define SPIM0_MISO_PIN 16


static const nrf_drv_spi_t m_spi_master = NRF_DRV_SPI_INSTANCE(0);

void spi_master_init(nrf_drv_spi_t const * p_instance, bool lsb);

void spiInit();

int8_t NRF51_SPI_writeRegister(uint8_t reg_addr, uint8_t * p_data, uint8_t nr_of_bytes, uint8_t cs_pin);

int8_t NRF51_SPI_readRegister(uint8_t address, uint8_t * p_data, uint8_t nr_of_bytes, uint8_t cs_pin);




#endif /* NRF51_SPI_H_ */
