#include "ble_functions.h"


uint16_t   m_conn_handle = BLE_CONN_HANDLE_INVALID;   /**< Handle of the current connection. */
static ble_uuid_t m_adv_uuids[] = {{BLE_UUID_BALL_SERVICE,         BLE_UUID_TYPE_VENDOR_BEGIN}}; /**< Universally unique service identifiers. */


/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    DEBUG_PRINTF("gap_params_init");
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}

void services_init(void)
{
    uint32_t       err_code;
    err_code = custom_service_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            DEBUG_PRINTF("BLE_ADV_EVT_FAST");
            break;
        case BLE_ADV_EVT_IDLE:
            DEBUG_PRINTF("BLE_ADV_EVT_IDLE");
            break;
        default:
            break;
    }
}


/**@brief Function for handling the Application's BLE Stack Write events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
void on_ble_write_evt(ble_evt_t * p_ble_evt)
{

    if(p_ble_evt->evt.gatts_evt.params.write.context.srvc_uuid.uuid != BLE_UUID_BALL_SERVICE) return;
    uint8_t input_data[3];
    switch (p_ble_evt->evt.gatts_evt.params.write.context.char_uuid.uuid)
            {
        case BLE_UUID_BATTERY_INTERVAL_CHAR:
        	client_next_battery_data_interval_s_request = *(p_ble_evt->evt.gatts_evt.params.write.data);
        	client_next_state_request = STATE_CHANGE_INTERVAL;
            break;
        case BLE_UUID_SENSOR_INTERVAL_CHAR:
        	client_next_sensor_data_interval_ms_request = *(p_ble_evt->evt.gatts_evt.params.write.data);
        	client_next_state_request = STATE_CHANGE_INTERVAL;
            break;
        case BLE_UUID_BALL_STATE_CHAR:
        	client_next_state_request = *(p_ble_evt->evt.gatts_evt.params.write.data);
            break;
        case BLE_UUID_LED_COLOR_CHAR:

        	client_led_color_request[0] = *(p_ble_evt->evt.gatts_evt.params.write.data);
        	client_led_color_request[1] = *(p_ble_evt->evt.gatts_evt.params.write.data+1);
        	client_led_color_request[2] = *(p_ble_evt->evt.gatts_evt.params.write.data+2);
        	client_next_state_request = STATE_COLOR_CHANGE;
        	break;
        default:
        	break;
    }
}


/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t err_code;

    switch (p_ble_evt->header.evt_id)
            {
        case BLE_GAP_EVT_CONNECTED:
            DEBUG_PRINTF("BLE_GAP_EVT_CONNECTED");
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break;
        case BLE_GAP_EVT_DISCONNECTED:
            DEBUG_PRINTF("BLE_GAP_EVT_DISCONNECTED");
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;
        case BLE_GAP_EVT_TIMEOUT:
            DEBUG_PRINTF("BLE_GAP_EVT_TIMEOUT");
            break;
        case BLE_GATTS_EVT_WRITE:
            DEBUG_PRINTF("BLE_GATTS_EVT_WRITE");
            on_ble_write_evt(p_ble_evt);
            break;
        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0,0);
            APP_ERROR_CHECK(err_code);
            break;
        default:
            // No implementation needed.
            break;
    }
}



/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    ble_conn_params_on_ble_evt(p_ble_evt);
    on_ble_evt(p_ble_evt);
    ble_advertising_on_ble_evt(p_ble_evt);
}


/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
void sys_evt_dispatch(uint32_t sys_evt)
{
    ble_advertising_on_sys_evt(sys_evt);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
void ble_stack_init(void)
{
    uint32_t err_code;

    DEBUG_PRINTF("ble_stack_init");
    // Initialize the SoftDevice handler module.

    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_RC_250_PPM_4000MS_CALIBRATION, false);
	// Enable BLE stack.
    ble_enable_params_t ble_enable_params;
    memset(&ble_enable_params, 0, sizeof(ble_enable_params));
    ble_enable_params.gatts_enable_params.service_changed = 1;
    err_code = sd_ble_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Advertising functionality.
 */
void advertising_init(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;

    DEBUG_PRINTF("advertising_init");
    // Build advertising data struct to pass into @ref ble_advertising_init.
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type               = BLE_ADVDATA_FULL_NAME;
    advdata.include_appearance      = true;
    advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    advdata.uuids_complete.p_uuids  = m_adv_uuids;

    ble_adv_modes_config_t options = {0};
    options.ble_adv_fast_enabled  = BLE_ADV_FAST_ENABLED;
    options.ble_adv_fast_interval = APP_ADV_INTERVAL;
    options.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;

    err_code = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL);
    APP_ERROR_CHECK(err_code);
}
