/*
 * led_functions.h
 *
 *  Created on: 17.04.2017
 *      Author: piter
 */

#ifndef LED_FUNCTIONS_H_
#define LED_FUNCTIONS_H_

#include "nrf.h"
#include "nrf_drv_config.h"
#include "app_error.h"
#include "app_pwm.h"
#include "pwm_functions.h"
#include "timers.h"
#include "global_defines.h"
#include "state_definitions.h"
#include "buzzer_functions.h"


#define SEARCH_STATE_LED_INTERVAL_MS 	200
#define TRIGGER_BALL_STATE_LED_INTERVAL_MS 200
#define ERROR_STATE_LED_INTERVAL_MS 	200

extern const app_pwm_t    PWM_R_AND_G_INSTANCE;
extern const app_pwm_t    PWM_B_INSTANCE;
extern const app_pwm_t    PWM_BUZ_INSTANCE;

extern volatile uint8_t client_led_color_request[3];

void setLEDColor( uint8_t R, uint8_t G, uint8_t B);
void turnOffLED();

void doStateLEDAction( ball_state_t state);

void ledTimerHandler();

void initLEDSequence();
void searchLEDSequence();
void errorLEDSequence();
void playLEDSequence();
void colorChangeSequence();
void triggerBallLEDSequence();

#endif /* LED_FUNCTIONS_H_ */
