/*
 * ble_functions.h
 *
 *  Created on: 11 maj 2016
 *      Author: Piotrek
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "nrf51_bitfields.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "boards.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "app_trace.h"
#include "bsp.h"
#include "nrf_delay.h"
#include "bsp_btn_ble.h"


#include "custom_peripheral_service.h"
#include "state_definitions.h"
#include "led_functions.h"

#ifndef BLE_FUNCTIONS_H_
#define BLE_FUNCTIONS_H_

#define DEVICE_NAME                      "Ball"                               /**< Name of device. Will be included in the advertising data. */
#define APP_ADV_INTERVAL                 300                                        /**< The advertising interval (in units of 0.625 ms. This value corresponds to 25 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS       0                                         /**< The advertising timeout in units of seconds. */

#define MIN_CONN_INTERVAL                MSEC_TO_UNITS(5, UNIT_1_25_MS)           /**< Minimum acceptable connection interval (0.1 seconds). */
#define MAX_CONN_INTERVAL                MSEC_TO_UNITS(8, UNIT_1_25_MS)           /**< Maximum acceptable connection interval (0.5 second). */
#define SLAVE_LATENCY                    6                                          /**< Slave latency. */
#define CONN_SUP_TIMEOUT                 MSEC_TO_UNITS(4000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */

/*debug functions*/
#ifdef DEBUG
#define DEBUG_PRINT 1
#else
#define DEBUG_PRINT 0
#endif
#define DEBUG_PRINTF(...) do{ if (DEBUG_PRINT) {printf(__VA_ARGS__); printf("\r\n"); }} while(0)


extern uint16_t   m_conn_handle;

/*functions*/
//static void uart_error_handle(app_uart_evt_t * p_event);
void ble_stack_init(void);
void device_manager_init(bool erase_bonds);
void gap_params_init(void);
void advertising_init(void);
void services_init(void);



#endif /* BLE_FUNCTIONS_H_ */
